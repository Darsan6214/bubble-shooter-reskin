﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class OffersPannelScript : MonoBehaviour {
    public Text timeWaitText;
    public GameObject coinsBundleGO;
    public DateTime offerTime;
	// Use this for initialization
	void Start () {
        
	}
	
	// Update is called once per frame
	void Update () {
        TimeSpan timespan = PrefManager.TimeLeft();
        if (timespan.Ticks > 0)
            timeWaitText.text = timespan.Days + "day " + timespan.Hours + "H " + timespan.Minutes + "M " + timespan.Seconds + "S";
        else
        {
            gameObject.SetActive(false);
            PrefManager.DeactivateOffer();
        }
	}

    

    public void BuyNowButton()
    {
        coinsBundleGO.SetActive(true);
        gameObject.SetActive(false);
    }

    public void CloseButton()
    {
        gameObject.SetActive(false);
    }
}
