﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class BuyCoinsScript : MonoBehaviour {
    
    public enum Mode
    {
        MENU,PLAY
    }

    public GameObject tilePref;
    public Transform scrollParent;
    
    public Mode mode;

    private GameScriptable gameScriptable;

    bool isInit;
    List<CoinsSet> coinsSetList;

    void Awake()
    {
        if(!isInit)
        {
            gameScriptable = DontDestroyOnLoadScript.instance.gameScriptable;
            isInit = true;
            coinsSetList = gameScriptable.InaappPurchaseCoins;
            SetupUI();
        }
    }

    void OnEnable()
    {
        if (mode == Mode.PLAY)
            GameplayController.instance.isOnHUD = true;
        InAppPurchaseScript.OnInappPurcharseFailed += OnInappPurchaseFailed;
        InAppPurchaseScript.OnInappSucessfully += OnInAppPurchaseSucessfully;
    }

    void OnDisable()
    {
        if (mode == Mode.PLAY)
            GameplayController.instance.isOnHUD = false;
        InAppPurchaseScript.OnInappPurcharseFailed -= OnInappPurchaseFailed;
        InAppPurchaseScript.OnInappSucessfully -= OnInAppPurchaseSucessfully;
    }

    void SetupUI()
    {
        
        for(int i=0;i<coinsSetList.Count;i++)
        {
            CoinsSet set = coinsSetList[i];
           GameObject go = Instantiate(tilePref);
            go.transform.parent = scrollParent;
            go.transform.localScale = Vector3.one;
            go.transform.Find("Buy/dollers").GetComponent<Text>().text = set.dollers;
            go.transform.Find("coins").GetComponent<Text>().text = set.coinsValue;
            Debug.Log(JsonUtility.ToJson(set));
            go.transform.Find("Buy").GetComponent<Button>().onClick.AddListener(()=>BuyButtonClick(set.inappId));           
        }
    }

    void BuyButtonClick(string str)
    {
        Debug.Log("Buy Button Click:" + str);
        InAppPurchaseScript.instance.BuyProductID(str);
    }


    void OnInAppPurchaseSucessfully(CoinsSet set)
    {
        PrefManager.AddCoins(int.Parse(set.coinsValue));
        if (mode == Mode.PLAY)
            GameplayController.instance.UpdateCoinsWithAnimation(.5f);
        else
            Camera.main.GetComponent<HomeScene>().UpdateCoinsWithAnimation(.5f);
        gameObject.SetActive(false);
    }

    void OnInappPurchaseFailed()
    {

    }
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void CloseButton()
    {
        gameObject.SetActive(false);
    }

}
