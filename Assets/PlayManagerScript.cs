﻿using UnityEngine;
using System.Collections;
using PlayFab;
using PlayFab.ClientModels;
using System.Collections.Generic;
using System;

public class PlayManagerScript : MonoBehaviour {
    public static PlayManagerScript instance;
    public delegate void ConnectionDelegate();
    public static ConnectionDelegate OnConnectionSucessfully;
    public static ConnectionDelegate OnConnectionFailed;
    
    public static bool isLogin;
    public static string normalLevelStr = "LEVEL";
    public static string endlessLevelStr = "ENDLESS_LEVEL";

    public static string TitleId = "F729";
    public static string PlayFabId;
    public static List<TopScoreUser> LEADERS_BOARD; 
    public int StaticUpdate { get; private set; }

    public Dictionary<string, string> facebookIdsForPlayfabId = new Dictionary<string, string>();
    public Dictionary<string,TopScoreUser> topScoreuserDict = new Dictionary<string, TopScoreUser>();

    
    void Awake()
    {
        if (instance == null)
        {
            instance = this;
            isLogin = false;
            PlayFabSettings.TitleId = TitleId;
        }
        else if(instance != this)
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);
        
       // Login();
    }

    public TopScoreUser GetUserById(string id)
    {
        if(topScoreuserDict.ContainsKey(id))
        return topScoreuserDict[id];

        return null;
    }

    public void Login(string acessToken)
    {
        
        LoginWithFacebookRequest request = new LoginWithFacebookRequest()
        {
            TitleId = PlayManagerScript.TitleId,
            CreateAccount = true,
            AccessToken = acessToken
            
        };


        Debug.Log("Logging to Playfab with "+acessToken );

        PlayFabClientAPI.LoginWithFacebook(request, (result) =>
        {
            PlayFabId = result.PlayFabId;
            isLogin = true;
            Debug.Log("Got PlayFabID: " + PlayFabId);

            if (result.NewlyCreated)
            {
                Debug.Log("(new account)");
            }
            else
            {
                Debug.Log("(existing account)");
            }

            OnLogin(true);
            //GetPlayerStatResult("Darsan");
            // UpdateStatic("Vijay", 111000);
            
        },
        (error) =>
        {
            Debug.Log("Error logging in player with custom ID:");
            Debug.Log(error.ErrorMessage);
            OnLogin(false);
        }
        
        );

    }

    private void OnLogin(bool v)
    {
        Debug.Log("LogIn Sucessfully");
        if(v)
        {
            List<string> idList = new List<string>(topScoreuserDict.Keys);
            
            GetPrefIdforFacebookIds(idList);
        }
        else
        {
            if (OnConnectionFailed != null)
                OnConnectionFailed();
        }
    }

    public void LoginAfterFacebook(string tokenString, List<TopScoreUser> list)
    {
        Debug.Log("Logging Facebook");
        list.ForEach((result) => { Debug.Log(result); });
        foreach(TopScoreUser top in list)
        {
            topScoreuserDict.Add(top.userId, top);
        }
        Login(tokenString);
    }

    void GetLeadersBoard(string str,Action<bool,List<LeadersboardUser>> Act=null)
    {
        if (!isLogin)
            return;
        GetFriendLeaderboardRequest request = new GetFriendLeaderboardRequest
        {
            IncludeFacebookFriends = true,
            StatisticName = str,
            MaxResultsCount = 10
        };

        PlayFabClientAPI.GetFriendLeaderboard(request, (result)=>{
            List<PlayerLeaderboardEntry> list = result.Leaderboard;
            List<LeadersboardUser> leadersboardList = new List<LeadersboardUser>();
            foreach(PlayerLeaderboardEntry entry in list)
            {
                leadersboardList.Add(new LeadersboardUser(entry.PlayFabId, entry.StatValue.ToString()));
                Debug.Log("Display_Name:" + entry.DisplayName
                    + " Position:" + entry.Position + " Value" + entry.StatValue);
            }

            if(Act != null)
            {
                Act(true, leadersboardList);
            }
            OnLeadersboardRecieved(true, leadersboardList);

        },(error)=>{
            if (Act != null)
            {
                Act(false,null);
            }
            OnLeadersboardRecieved(false);
            Debug.Log(error.ErrorMessage);
        });
    }

    private void OnLeadersboardRecieved(bool isSucess, List<LeadersboardUser> leadersboardList=null)
    {
        if(isSucess)
        {

        }
    }

    void GetPrefIdforFacebookIds(List<string> idList)
    {
        if (!isLogin)
            return;
        GetPlayFabIDsFromFacebookIDsRequest request = new GetPlayFabIDsFromFacebookIDsRequest
        {
            FacebookIDs = idList
        };


        PlayFabClientAPI.GetPlayFabIDsFromFacebookIDs(request, (result) => {
           List<FacebookPlayFabIdPair> list = result.Data;
            Dictionary<string, string> dict = new Dictionary<string, string>();
            foreach (FacebookPlayFabIdPair pair in list)
            {
                dict.Add(pair.PlayFabId,pair.FacebookId);
            }
            OnFacebookIdsRecieved(true,dict);
        }, (error) => {
            OnFacebookIdsRecieved(false);
        });
    }

  

    void OnFacebookIdsRecieved(bool isSucess,Dictionary<string,string> ids=null)
    {
        Debug.Log("Facebook ids recieved:" + isSucess);
        if (isSucess)
        {
            facebookIdsForPlayfabId = ids;
            if (OnConnectionSucessfully != null)
                OnConnectionSucessfully();
        }
        else
        {
            if (OnConnectionFailed != null)
                OnConnectionFailed();
        }
       // 
    }


    // Use this for initialization
    void Start () {

	}

    void UpdateStatic(string statname,int val)
    {
        if (!isLogin)
            return;
        UpdatePlayerStatisticsRequest request = new UpdatePlayerStatisticsRequest
        {
            // List < StatisticUpdate > list ;
            Statistics = new List<StatisticUpdate>()
            {
                new StatisticUpdate()
                {
                    StatisticName = statname,
                    Value = val
                }
            }
        };

        PlayFabClientAPI.UpdatePlayerStatistics(request, (result) => {
            OnUpdateStatics(true);
            Debug.Log(JsonUtility.ToJson(result));
        },
        (error) => {
            OnUpdateStatics(false);
            Debug.Log(error.ErrorMessage);
        });

    }

    void OnUpdateStatics(bool isSucess)
    {

    }

    void GetPlayerStatResult(string str)
    {
        if (!isLogin)
            return;
        GetPlayerStatisticsRequest request = new GetPlayerStatisticsRequest
        {

            StatisticNames = new List<string>()
            {
                str
            }
        };

        PlayFabClientAPI.GetPlayerStatistics(request, (result)=>{
            List<StatisticValue> valueList = result.Statistics;
            foreach(StatisticValue v in valueList)
            {
                Debug.Log(v.Value);
            }
            OnPlayerGetResult(true, valueList.Count == 0 ? -1 : valueList[0].Value);

        },(error)=>{
            OnPlayerGetResult(false);
            Debug.Log(error.ErrorMessage);
        });
        
    }

    public TopScoreUser GetTopScoreUserByLeadersBoardUser(LeadersboardUser user)
    {
        if (facebookIdsForPlayfabId.ContainsKey(user.playfabId))
        {
            string i = facebookIdsForPlayfabId[user.playfabId];
            if (topScoreuserDict.ContainsKey(i))
            {
                TopScoreUser t = topScoreuserDict[i];
                t.score = user.value;
                return t;
            }
        } 

        return null;
    }

    public void SaveScoreForLevel(int i,int score,bool isPuzzle)
    {
        UpdateStatic((isPuzzle ? normalLevelStr : endlessLevelStr) + i, score);
    }


    void OnPlayerGetResult(bool isSucess, int value=0)
    {

    }

    public bool GetLeadersBoardForLevel(int i,bool isPuzzle)
    {
        LEADERS_BOARD = null;
        if (!isLogin)
            return false;
        
        GetLeadersBoard((isPuzzle ? normalLevelStr : endlessLevelStr)+i, (isSucess, result) =>
             {
                 if (isSucess)
                 {
                     LEADERS_BOARD = new List<TopScoreUser>();
                     for (int j = 0; j < result.Count; j++)
                     {
                         LEADERS_BOARD.Add(GetTopScoreUserByLeadersBoardUser(result[j]));
                     }
                 }
             }

        );
        return true;
    }

    public void UpdateImage(string id,Sprite sp)
    {
        if (!topScoreuserDict.ContainsKey(id))
            return;
        topScoreuserDict[id].img = sp;
    }
	
	// Update is called once per frame
	void Update () {
	
	}
}


public class LeadersboardUser
{
   public string playfabId;
   public string value;

    public LeadersboardUser(string playfabId,string value)
    {
        this.playfabId = playfabId;
        this.value = value;
    }
}