﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class BuyItemsPannelScript : MonoBehaviour {
    public delegate bool GameAction();
    public static GameAction OnBombBuyButtonClicked;
    public static GameAction OnMonosterBuyButtonClicked;
    public GameObject bombPopUpGO;
    public GameObject monosterPopUpGO;

    public Text bombCountTxt;
    public Text monosterCountTxt;

    private bool isOnHud;
    // Use this for initialization
    void Start () {
	
	}
	
    void OnEnable()
    {
        UpdateBombTxt();
        UpdateMonosterTxt();
    }

    public void AddBombsButton()
    {
        isOnHud = true;
        bombPopUpGO.SetActive(true);
        OnBombBuyButtonClicked += BuyBomb;
    }

    public void AddMonosterButton()
    {
        isOnHud = true;
        monosterPopUpGO.SetActive(true);
        OnMonosterBuyButtonClicked += BuyMonoster;
    }

    public void BombButtonClicked()
    {
        if (PrefManager.Bomb.GetBomb() == 0)
        {
            isOnHud = true;
            bombPopUpGO.SetActive(true);
            OnBombBuyButtonClicked += BuyBombAndUse;
            return;
        }

        if (!GameplayController.instance.SetAsBomb())
            return;

        PrefManager.Bomb.UseOneBomb();
        UpdateBombTxt();

    }



    public void MonosterButtonClicked()
    {
        if (PrefManager.Monoster.GetMonoster() == 0)
        {
            isOnHud = true;
            monosterPopUpGO.SetActive(true);
            OnMonosterBuyButtonClicked += BuyAndUseMonoster ;
            return;
        }

        if (!GameplayController.instance.SetAsMonoster())
            return;

        PrefManager.Monoster.UseOneMonoster();
        UpdateMonosterTxt();
    }

    void UpdateMonosterTxt()
    {
        monosterCountTxt.text = PrefManager.Monoster.GetMonoster() + "";
    }


    void UpdateBombTxt()
    {
        bombCountTxt.text = PrefManager.Bomb.GetBomb() + "";
    }

    public void BuyBombsButton()
    {
        if (OnBombBuyButtonClicked != null)
        {
            if (OnBombBuyButtonClicked())
            {
                OnBombBuyButtonClicked = null;
            }
        }
    }


    bool BuyBomb()
    {
        if (PrefManager.GetCoins() >= 200)
        {
            PrefManager.SetCoins(PrefManager.GetCoins() - 200);
            PrefManager.Bomb.AddBombs(2);
            UpdateBombTxt();
            GameplayController.instance.UpdateCoinsWithAnimation(.5f);
            CloseBombPopup();
            
            return true;
        }
        else
        {
            GameSceneController.instance.CoinsPannel();
            return false;
        }
    }

    bool BuyMonoster()
    {
        if (PrefManager.GetCoins() >= 300)
        {
            PrefManager.SetCoins(PrefManager.GetCoins() - 300);
            PrefManager.Monoster.AddMonosters(2);
            UpdateMonosterTxt();
            GameplayController.instance.UpdateCoinsWithAnimation(.5f);
            CloseMonosterPopup();

            return true;
        }
        else
        {
            GameSceneController.instance.CoinsPannel();
            return false;
        }
    }

    bool BuyAndUseMonoster()
    {
        if (PrefManager.GetCoins() >= 300)
        {
            PrefManager.SetCoins(PrefManager.GetCoins() - 300);
            PrefManager.Monoster.AddMonosters(2);
            UpdateMonosterTxt();
            GameplayController.instance.UpdateCoinsWithAnimation(.5f);
            CloseMonosterPopup();
            MonosterButtonClicked();
            return true;
        }
        else
        {
            GameSceneController.instance.CoinsPannel();
            return false;
        }
    }

    bool BuyBombAndUse()
    {
        if (PrefManager.GetCoins() >= 200)
        {
            PrefManager.SetCoins(PrefManager.GetCoins() - 200);
            PrefManager.Bomb.AddBombs(2);
            UpdateBombTxt();
            GameplayController.instance.UpdateCoinsWithAnimation(.5f);
            CloseBombPopup();
            BombButtonClicked();
            return true;
        }
        else
        {
            GameSceneController.instance.CoinsPannel();
            return false;
        }
    }

    public void BuyMonosterButton()
    {
        if(OnMonosterBuyButtonClicked != null)
        {
           if(OnMonosterBuyButtonClicked())
            {
                OnMonosterBuyButtonClicked = null;
            }
        }
    }




    public void CloseBombPopup()
    {
        bombPopUpGO.SetActive(false);
        isOnHud = false;
        GameplayController.instance.isOnHUD = false;
    }

    public void CloseMonosterPopup()
    {
        monosterPopUpGO.SetActive(false);
        isOnHud = false;
        GameplayController.instance.isOnHUD = false;
    }


	// Update is called once per frame
	void Update () {
        if(isOnHud)
        GameplayController.instance.isOnHUD = true;
    }
}
