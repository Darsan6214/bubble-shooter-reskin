﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using Facebook.MiniJSON;

public class TopScoreUserUI : MonoBehaviour {

    TopScoreUser topscoreuser;
    public Image userImage;
    public Text userNameTxt;
    public Text scoreTxt;

    private string userId;
    private bool isInit;
	// Use this for initialization
	void Start () {
        isInit = true;
        if(topscoreuser != null)
        {
            if (FacebookManager.instance.IsLogIn() && topscoreuser.img == null )
            {
                StartCoroutine(GetImage());
            }
            else if(userId == FacebookManager.instance.userid)
            {
                userImage.sprite = FacebookManager.instance.userImage;
            }
        }
	}

    public void UpdateDetails(TopScoreUser topscoreuser)
    {
        this.topscoreuser = topscoreuser;
        userNameTxt.text = topscoreuser.userName;
        scoreTxt.text = topscoreuser.score;
        userId = topscoreuser.userId;
        if(FacebookManager.instance.IsLogIn() && topscoreuser.img == null && isInit)
        {
            StartCoroutine(GetImage());
        }
        else if(topscoreuser.img != null)
        {
            userImage.sprite = topscoreuser.img;
        }

    }

    IEnumerator GetImage()
    {
        string url = "https://graph.facebook.com/"+userId+
            "/picture?type=large&width=128&height=128&redirect=false";

        WWW www = new WWW(url);
        yield return www;
        if(www.error == null)
        {
            Dictionary<string, object> dict = Json.Deserialize(www.text) as Dictionary<string, object>;
            Dictionary<string, object> dataDict = dict["data"] as Dictionary<string, object>;
            StartCoroutine(DownloadImage(dataDict["url"].ToString()));

        }
        

    }
	

    IEnumerator DownloadImage(string url)
    {
        WWW wwwImage = new WWW(url);
        yield return wwwImage;
        if (wwwImage.error == null)
            userImage.sprite = Sprite.Create(wwwImage.texture, new Rect(0, 0, 128, 128), new Vector2(.5f, .5f));
        PlayManagerScript.instance.UpdateImage(userId, userImage.sprite);
        if(userId == FacebookManager.instance.userid)
        {
            FacebookManager.instance.SaveMyUserImage(wwwImage.texture);
        }
    }
	// Update is called once per frame
	void Update () {
	
	}
}
