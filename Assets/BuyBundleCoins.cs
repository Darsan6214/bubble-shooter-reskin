﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class BuyBundleCoins : MonoBehaviour {

    public enum Mode
    {
        PLAY, MENU,
    }

    private GameScriptable scriptable;
    public GameObject scrollPannelParent;
    public GameObject coinsBundleTilePref;
    private List<CoinsBundleSet> coinsSetBundleList;
    public Mode mode;
    
    void Awake()
    {
        scriptable = DontDestroyOnLoadScript.instance.gameScriptable;
    }

	// Use this for initialization
	void Start () {
        coinsSetBundleList = scriptable.inappPurchaseCoinsForOffer;
        for (int i=0;i<coinsSetBundleList.Count;i++)
        {
            CoinsBundleSet set = coinsSetBundleList[i];
            GameObject go =  Instantiate(coinsBundleTilePref) as GameObject;

            
            go.transform.FindChild("Buy/Panel/dollerTxt").GetComponent<Text>().text = set.dollers;
            
            go.transform.FindChild("multiplexerTxt").GetComponent<Text>().text = set.multiPlexer + "x";
            go.transform.FindChild("Buy").GetComponent<Button>().onClick.AddListener(()=>BuyButtonClicked(set.inappId));
            go.transform.parent = scrollPannelParent.transform;
            go.transform.localScale = Vector3.one;
            go.transform.FindChild("coinsTxt").GetComponent<Text>().text = set.coinsValue;
        }
	    
	}

    void OnEnable()
    {
        InAppPurchaseScript.OnInappSucessfully += OnInappSucess;
        InAppPurchaseScript.OnInappPurcharseFailed += OnInappFailed;
    }

    void OnDisable()
    {
        InAppPurchaseScript.OnInappSucessfully -= OnInappSucess;
        InAppPurchaseScript.OnInappPurcharseFailed -= OnInappFailed;
    }

    void BuyButtonClicked(string id)
    {
        InAppPurchaseScript.instance.BuyProductID(id);
    }

    void OnInappSucess(CoinsSet set)
    {
        gameObject.SetActive(false);
        CoinsBundleSet bundleSet = (CoinsBundleSet)set;
        PrefManager.AddCoins(int.Parse(bundleSet.coinsValue) * bundleSet.multiPlexer);
        if(mode == Mode.PLAY)
        {
            GameplayController.instance.UpdateCoinsWithAnimation(.5f);
        }
        Debug.Log("OnInapp Sucess :" + bundleSet.inappId);
    }

    public void CloseButton()
    {
        gameObject.SetActive(false);
            
    }

    void OnInappFailed()
    {

    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
