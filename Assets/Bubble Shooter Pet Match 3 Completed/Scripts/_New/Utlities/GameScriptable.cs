﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class GameScriptable : ScriptableObject
{
    [Header("Game")]
    public string gameLink;

    [Header("Play Count Between Rate")]
    public int count;

    [Header("Inapp Purcharse")]
    public List<CoinsSet> InaappPurchaseCoins;

    [Header("Offer")]
    public bool hasOffer;
    public Delay delay;
    public List<CoinsBundleSet> inappPurchaseCoinsForOffer;
}

[System.Serializable]
public class Delay
{
    public int days;
    public int hours;
    public int minutes;
    public int seconds;
}

