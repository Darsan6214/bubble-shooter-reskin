﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class CoinsSet {
    public string inappId;
    public string coinsValue;
    public string dollers; 
}
