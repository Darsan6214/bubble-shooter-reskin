﻿using UnityEngine;
using System.Collections;
using System;

public class PrefManager : MonoBehaviour {
    public static string coinsStr = "COINS";
    public static string puzzleLevelPlayed = "PUZZLELEVEL";
    public static string endlessLevelPlayed = "ENDLESSLEVEL";
    public static string hasOfferstr = "HAS_OFFER";
    public static string offerTime = "OFFER_TIME";
    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public static class Rate
    {
        public static string rateStr = "RATE";
        public static string rateMeCountDownStr = "RATEME_COUNTDOWN";
        public static bool AddCountAndCheckOfRating()
        {
            PlayerPrefs.SetInt(rateStr, PlayerPrefs.GetInt(rateStr, 0) + 1);

            if(PlayerPrefs.GetInt(rateStr)>=GetRateMeCountDown())
            {
                PlayerPrefs.SetInt(rateStr, 0);
                return true;
            }
            return false;
        }

        public static void SetRateMeCountDown(int countDown)
        {
            PlayerPrefs.SetInt(rateMeCountDownStr, countDown);
            PlayerPrefs.SetInt(rateStr, 0);
        }

        public static int GetRateMeCountDown()
        {
            if(!PlayerPrefs.HasKey(rateMeCountDownStr))
            {
                PlayerPrefs.SetInt(rateMeCountDownStr, 2);
            }

            return PlayerPrefs.GetInt(rateMeCountDownStr);
        }
    }

    public static class Bomb
    {
        public static string bombStr = "BOMB";
        public static void AddBombs(int bomb)
        {
            PlayerPrefs.SetInt(bombStr, PlayerPrefs.GetInt(bombStr) + bomb);
        }

        public static int GetBomb()
        {
            if (!PlayerPrefs.HasKey(bombStr))
                PlayerPrefs.SetInt(bombStr, 2);

            if (PlayerPrefs.GetInt(bombStr) < 0)
                PlayerPrefs.SetInt(bombStr,0);

            return PlayerPrefs.GetInt(bombStr);
        }

        public static bool UseOneBomb()
        {
            if (GetBomb() >= 1)
            {
                PlayerPrefs.SetInt(bombStr, PlayerPrefs.GetInt(bombStr, 2) - 1);
                return true;
            }
            return false;
            
        }

        public static void UpdateBomb(int count)
        {
            PlayerPrefs.SetInt(bombStr, count);
        }
    }


    public static class Monoster
    {
        public static string monosterStr = "MONOSTER";
        public static void AddMonosters(int monoster)
        {
            PlayerPrefs.SetInt(monosterStr, PlayerPrefs.GetInt(monosterStr) + monoster);
        }

        public static int GetMonoster()
        {
            if (!PlayerPrefs.HasKey(monosterStr))
                PlayerPrefs.SetInt(monosterStr, 2);

            if (PlayerPrefs.GetInt(monosterStr) < 0)
                UpdateMonoster(0);

            return PlayerPrefs.GetInt(monosterStr);
        }

        public static bool UseOneMonoster()
        {
            if (PlayerPrefs.GetInt(monosterStr, 2) >= 1)
            {
                PlayerPrefs.SetInt(monosterStr, PlayerPrefs.GetInt(monosterStr, 2) - 1);
                return true;
            }
            return false;
        }

        public static void UpdateMonoster(int count)
        {
            PlayerPrefs.SetInt(monosterStr, count);
        }
    }

    public static void SetCoins(int coins)
    {
        PlayerPrefs.SetInt(coinsStr, coins);
    }

    public static void AddCoins(int coins)
    {
        SetCoins(GetCoins() + coins);
    }

    public static void SpendCoins(int coins)
    {
        SetCoins(GetCoins() - coins);
    }

    public static void SetLevelPlayed(int lev,bool isPuzzle)
    {
        PlayerPrefs.SetInt((isPuzzle ? puzzleLevelPlayed : endlessLevelPlayed) + lev, 1);
    }

    public static bool GetLevelPlayed(int lev,bool isPuzzle)
    {
        return PlayerPrefs.GetInt((isPuzzle ? puzzleLevelPlayed : endlessLevelPlayed)+lev, 0) == 1 ? true : false;
    }

    public static bool HasOffer()
    {
        long temp = long.Parse(PlayerPrefs.GetString(offerTime, ""));
        TimeSpan timeLeft = DateTime.FromBinary(temp).Subtract(DateTime.Now);
        if (timeLeft.Ticks < 0)
            PlayerPrefs.SetInt(hasOfferstr, 0);
        return PlayerPrefs.GetInt(hasOfferstr, 0) == 1;
    }

    public static void DeactivateOffer()
    {
        PlayerPrefs.SetInt(hasOfferstr, 0);
    }

    public static TimeSpan TimeLeft()
    {
        long temp = long.Parse(PlayerPrefs.GetString(offerTime,""));
        TimeSpan timeLeft = DateTime.FromBinary(temp).Subtract(DateTime.Now);


            return timeLeft;

    }

    public static void SetOffer(int day,int hour,int min)
    {
        PlayerPrefs.SetInt(hasOfferstr, 1);
        DateTime dateAndtime =  DateTime.Now.AddDays(day).AddHours(hour).AddMinutes(min);
        PlayerPrefs.SetString(offerTime, dateAndtime.ToBinary().ToString());
    }

    public static int GetCoins()
    {
        if(!PlayerPrefs.HasKey(coinsStr))
        {
            PlayerPrefs.SetInt(coinsStr, 1000);
        }

        return PlayerPrefs.GetInt(coinsStr, 1000);
    }

}
