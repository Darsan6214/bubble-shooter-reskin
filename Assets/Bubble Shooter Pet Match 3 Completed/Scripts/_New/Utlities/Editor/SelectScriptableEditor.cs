﻿using UnityEngine;
using System.Collections;
using UnityEditor;

public class SelectScriptableEditor : Editor {

	[MenuItem("Bubbles/Setup")]
    public static void Setup()
    {
        Selection.activeObject = AssetDatabase.LoadAssetAtPath("Assets/Scriptable/GameScriptable.asset",typeof(GameScriptable));
        EditorUtility.FocusProjectWindow();
    }
}
