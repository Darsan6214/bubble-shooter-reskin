﻿using UnityEngine;
using System.Collections;
using UnityEditor;

public class CreateScriptableObject : MonoBehaviour {

    [MenuItem("Assets/Create/Coins Item List")]
    public static GameScriptable Create()
    {
        GameScriptable asset = ScriptableObject.CreateInstance<GameScriptable>();
        AssetDatabase.CreateAsset(asset, "Assets/Scriptable/GameScriptable.asset");
        AssetDatabase.SaveAssets();
        Selection.activeObject = asset;
        return asset;
    }

}
