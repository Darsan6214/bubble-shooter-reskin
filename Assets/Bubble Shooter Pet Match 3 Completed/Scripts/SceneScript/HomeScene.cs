﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;

public class HomeScene : MonoBehaviour
{

    public static bool EnterFirstTime = true;
    public GameObject LoadingText;
    public Image LoadingBar;
    float currentLoadPercent = 0;
    public GameObject UIs, Gang;
    public GameObject OptionMenu;
    public Text coinsTxt;
    public bool LoadDataFinished = false;
    public GameObject shopPannelGO;
    public GameObject facebookGO;
    public GameObject loadingPannel;

    void Start()
    {
        SetQuality();

        if (GlobalData.DataLoaded == false)
            StartCoroutine(LoadData());
        else
        {
            LoadDataFinished = true;
        }
        if(FacebookManager.instance.IsLogIn())
        {
            facebookGO.SetActive(false);
        }
    }

    void SetQuality()
    {
#if UNITY_IPHONE
        Application.targetFrameRate = 60;
#endif
    }

    void Update()
    {
        if (UIs.activeSelf == false)
        {
            UpdateLoading();
        }

        if(Input.GetKeyDown(KeyCode.Escape)){
            Application.Quit();
        }
    }

    void OnEnable()
    {
        UpdateCoinsWithoutAnimation();
        FacebookManager.OnConnectionFailed += OnConnectionFailed;
        PlayManagerScript.OnConnectionSucessfully += OnConnectedSucessfully;
        PlayManagerScript.OnConnectionFailed += OnConnectionFailed;
    }

    void OnDisable()
    {
        FacebookManager.OnConnectionFailed -= OnConnectionFailed;
        PlayManagerScript.OnConnectionSucessfully -= OnConnectedSucessfully;
        PlayManagerScript.OnConnectionFailed -= OnConnectionFailed;
    }

    void UpdateLoading()
    {
        if (currentLoadPercent >= 100)
        {
            LoadingText.gameObject.SetActive(false);
            UIs.SetActive(true);
            Gang.SetActive(true);
            // Audio
            if (BaseManager.globalGameMusic != null)
            {
                StartCoroutine(AudioHelper.FadeAudioObject(BaseManager.globalGameMusic, -1.0f));
            }
            if (BaseManager.globalMenuMusic == null)
            {
                // create and return the Intro Scene music audio
                BaseManager.globalMenuMusic = AudioHelper.CreateGetFadeAudioObject(BaseManager.GetInstance().menuMusic, true, BaseManager.GetInstance().fadeClip, "Audio-MenuMusic");
                // play the clip
                StartCoroutine(AudioHelper.FadeAudioObject(BaseManager.globalMenuMusic, 0.5f));
            }
        }
        else
        {
            float loadpercent = 20;
            if (LoadDataFinished) loadpercent += 80;

            // update loading bar
            if (currentLoadPercent < loadpercent)
            {
                currentLoadPercent += 1;
                LoadingBar.fillAmount = currentLoadPercent / 100;
            }
        }
    }

    IEnumerator LoadData()
    {
        GlobalData.LoadLevelData();
        yield return new WaitForSeconds(1.0f);
        LoadDataFinished = true;
    }

    public void ShowOptionMenu()
    {
        OptionMenu.SetActive(true);
    }

    public void HideOptionMenu()
    {
        OptionMenu.SetActive(false);
    }

    public void GotoLevelScene()
    {
        // Reset load counter
        BranchPairSetup.ResetAllValue();

        Application.LoadLevel("LevelSelect");
    }

    public void StartEndlessMode()
    {
        // Reset load counter
        BranchPairSetup.ResetAllValue();

        GlobalData.gameMode = GlobalData.GameMode.ENDLESS_MODE;
        GlobalData.SetCurrentLevel(1);

        Application.LoadLevel("GameScene");
    }

    public void LoginFacebookButton()
    {
        //FacebookManager.instance.LogIn();
        FacebookManager.instance.LogIn();
        loadingPannel.SetActive(true);
    }

    public void ShopButton()
    {
        shopPannelGO.SetActive(true);
    }

    public void UpdateCoinsWithOutAnimation()
    {

    }

    void UpdateCoinsWithoutAnimation()
    {
        coinsTxt.text = PrefManager.GetCoins() + "";
    }

    public void UpdateCoinsWithAnimation(float time)
    {
        StartCoroutine(UpdateCoinsWithAnimationCor(time));
    }

    IEnumerator UpdateCoinsWithAnimationCor(float t)
    {
        int coins = PrefManager.GetCoins();
        float currentValue = int.Parse(coinsTxt.text);
        if (coins != int.Parse(coinsTxt.text))
        {
            float value = Mathf.Abs((.03f / t) * (coins - currentValue));

            if (coins > currentValue)
            {
                while (coins > currentValue)
                {
                    currentValue += value;
                    coinsTxt.text = (int)(Mathf.Clamp(currentValue, 0, coins)) + "";
                    yield return new WaitForSeconds(0.03f);
                }
                coinsTxt.text = coins + "";
            }
            else
            {
                while (coins < currentValue)
                {
                    currentValue -= value;
                    coinsTxt.text = (int)(Mathf.Clamp(currentValue, 0, coins)) + "";
                    yield return new WaitForSeconds(0.03f);
                }
                coinsTxt.text = coins + "";
            }
        }

    }

    void OnConnectedSucessfully()
    {
        facebookGO.SetActive(false);
        loadingPannel.SetActive(false);
    }

    void OnConnectionFailed()
    {
        loadingPannel.SetActive(false);
    }

}
