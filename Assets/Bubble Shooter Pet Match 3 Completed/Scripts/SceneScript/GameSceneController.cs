﻿using UnityEngine;
using System.Collections;

public class GameSceneController : MonoBehaviour
{
    public static GameSceneController instance;

    GameplayController gamePlayController;

    public GameObject FadeObject;

    public static bool needShowFade = false;


    void Awake()
    {
        instance = this;
    }

    void Start()
    {
        needShowFade = false;
        FadeObject.GetComponent<Animation>().Play("FadeIn");
        gamePlayController = GameObject.FindObjectOfType<GameplayController>();

        // Initial
        // Levelselect switch
        LevelSelectController.NewUnlockedLevel = -1;
        LevelSelectController.needUpdateMoveSquirrelIcon = false;
    }

    void Update()
    {
        if (needShowFade)
        {
            needShowFade = false;
            FadeObject.GetComponent<Animation>().Play("FadeOut");
        }
    }

    // Scene Controlling
    // Menu Popup Area
    public GameObject PauseMenu, WinMenu, EndlessWinMenu, LoseMenu, OptionMenu,coinsPannelGO,ratingPannelGO;
    public UnityEngine.UI.Button PauseButton;
    public void PauseGame()
    {
        if (gamePlayController.gamePaused)
        {
            gamePlayController.gamePaused = false;
            PauseMenu.gameObject.SetActive(false);
            PauseButton.enabled = true;
        }
        else
        {
            gamePlayController.gamePaused = true;
            PauseMenu.gameObject.SetActive(true);
            PauseButton.enabled = false;
        }
    }

    public void OpenSetting()
    {
        PauseMenu.gameObject.SetActive(false);
        OptionMenu.gameObject.SetActive(true);
    }

    public void CoinsPannel()
    {
        coinsPannelGO.SetActive(true);
    }

    public void CloseSetting()
    {
        PauseMenu.gameObject.SetActive(true);
        OptionMenu.gameObject.SetActive(false);
    }

    public void RestartLevel()
    {
        Application.LoadLevel("GameScene");
    }

    public void NextLevel()
    {
        if (GlobalData.gameMode == GlobalData.GameMode.PUZZLE_MODE)
        {
            LevelSelectController.ShowPlayDialog(GlobalData.GetCurrentLevel() + 1);
            GoToHomeScene();
        }
        else if (GlobalData.gameMode == GlobalData.GameMode.ENDLESS_MODE)
        {
            // Continue gameplay
            EndlessWinMenu.SetActive(false);

            gamePlayController.ContinuePlayEndless();
        }
    }


    public void RatingMeFunction()
    {
        ratingPannelGO.SetActive(true);
    }

    public void GoToHomeScene()
    {
        GameSceneController.needShowFade = true;

        // Reset load counter
        BranchPairSetup.ResetAllValue();

        if (GlobalData.gameMode == GlobalData.GameMode.PUZZLE_MODE)
        {
            Application.LoadLevel("LevelSelect");
        }
        else if (GlobalData.gameMode == GlobalData.GameMode.ENDLESS_MODE)
        {
            Application.LoadLevel("HomeScene");
        }
    }

}
