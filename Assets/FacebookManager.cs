﻿using UnityEngine;
using System.Collections;
using Facebook.Unity;
using System.Collections.Generic;
using System;
using Facebook.MiniJSON;
using System.IO;

public class FacebookManager : MonoBehaviour {
    public static FacebookManager instance;
    public delegate void ConnectionDelegate();
    public static ConnectionDelegate OnConnectSucessfully;
    public static ConnectionDelegate OnConnectionFailed;
    public static string levelscoreStr = "Levelscore";
    public bool isGet;
    List<TopScoreUser> topScoreUserList = null;
    public Sprite userImage;
    public string userName;
    public string userid;
    public AccessToken acessToken;
    
// Include Facebook namespace
void Awake()
{
    if (instance == null)
    {
        instance = this;
        userName = PlayerPrefs.GetString("username", "User");
        userid = PlayerPrefs.GetString("userid", "");
        userImage = GetUserImage();
            
       

        DontDestroyOnLoad(gameObject);
        if (!FB.IsInitialized)
        {
            // Initialize the Facebook SDK
            FB.Init(InitCallback, OnHideUnity);
        }
        else
        {
            // Already initialized, signal an app activation App Event
            FB.ActivateApp();

                if(FB.IsLoggedIn)
                {
                    AuthCallback(null);
                }
        }
    }
    else if (instance != this)
    {
        Destroy(gameObject);
    }
}

private void InitCallback()
{
    if (FB.IsInitialized)
    {
        FB.ActivateApp();
    }
    else
    {
        
    }
}

private void OnHideUnity(bool isGameShown)
{
    if (!isGameShown)
    {
        Time.timeScale = 0;
    }
    else
    {
        Time.timeScale = 1;
    }
}

    public void GetSavedMyImage()
    {
        try
        {
            var bytes = File.ReadAllBytes(Application.dataPath + "/" + "user");
            Texture2D tex = new Texture2D(128, 128);
            tex.LoadImage(bytes);
            userImage = Sprite.Create(tex, new Rect(0, 0, 128, 128), new Vector2(.5f, .5f));
            // new File.open(Application.dataPath + "/" + fileName, FileMode.Create);
        }
        catch (Exception e)
        {

        }
    }

   public void LogIn()
    {
        if (FB.IsInitialized && !FB.IsLoggedIn)
        {
            var perms = new List<string>() { "publish_actions", "public_profile", "user_friends","email" };
            FB.LogInWithPublishPermissions(perms, AuthCallback);
        }

    }

    public Sprite GetUserImage()
    {
        try
        {
          byte[] bytes =  File.ReadAllBytes(Application.dataPath + "/" + "user");
            Texture2D tex = new Texture2D(128, 128);
            tex.LoadImage(bytes);
            return  Sprite.Create(tex, new Rect(0, 0, 128, 128), new Vector2(.5f, .5f));
        }
        catch(Exception e)
        {

        }
        return null;
    }

    public void SaveMyUserImage(Texture2D texture)
    {
        try
            {
            userImage = Sprite.Create(texture, new Rect(0, 0, 128, 128), new Vector2(0.5f, .5f));
            var bytes = texture.EncodeToPNG();
                var file = File.Create(Application.dataPath + "/" + "user");
                // new File.open(Application.dataPath + "/" + fileName, FileMode.Create);
                var binary = new BinaryWriter(file);
                binary.Write(bytes);
                file.Close();
            }
            catch (Exception e)
            {

            }
        
    }

    public bool QueryScores()
    {
        if (!FB.IsLoggedIn)
            return false;
        isGet = false;
        FB.API("/app/scores?fields=" + "score" + ",user.limit(100)"
            , HttpMethod.GET, delegate (IGraphResult result)
            {
                //topScoreUserList = new List<TopScoreUser>();
                List<TopScoreUser> topScoreUser = new List<TopScoreUser>();
                var temp = (Dictionary < string, object>)Facebook.MiniJSON.Json.Deserialize(result.RawResult);
                
                Debug.Log(result.RawResult);
                if (temp != null)

                {
                    List<string> testList = new List<string>(temp.Keys);
                    testList.ForEach((item) => { Debug.Log(item); });
                    Debug.Log(testList.Count);
                    var tempList = (List<object>)temp["data"];
                    for (int i = 0; i < tempList.Count; i++)
                    {
                        object obj = tempList[i];
                        
                        var entry = (Dictionary<string, object>)obj;
                        var user = (Dictionary<string, object>)entry["user"];
                        Debug.Log("FriendsList " + (string)user["name"]);
                        topScoreUser.Add(new TopScoreUser((string)user["name"], (string)user["id"]));
                    }
                    OnPlayerListRecieved(true, topScoreUser);
                }
                isGet = true;
                if (result.Error != null)
                {
                    OnPlayerListRecieved(false);
                    Debug.Log(result.Error);
                }
            });//ScoresCallback);
        return true;
    }

    void OnPlayerListRecieved(bool isSucess,List<TopScoreUser> list = null)
    {
        Debug.Log("Friends List Recieved " + isSucess);
        //playfab works
        if (isSucess)
        {
            PlayManagerScript.instance.LoginAfterFacebook(acessToken.TokenString,list);
        }
    }


    public bool SetPlayerOnFacebook()
    {
        if (!FB.IsLoggedIn)
            return false;
        Dictionary<string, string> scoreData = new Dictionary<string, string>();
        scoreData["score"] = "1";
        FB.API("/me/scores", HttpMethod.POST, delegate (IGraphResult result) {

            if (result.Error == null) QueryScores();
            else if (OnConnectionFailed != null) OnConnectionFailed();
            Debug.Log("Player Setuped in Facebook");
            Debug.Log(result.Error);
        }, scoreData);
        return true;
    }


    
    public List<TopScoreUser> GetTopUserList()
    {
        isGet = false;
        return topScoreUserList;
    }

    private void AuthCallback(ILoginResult result)
    {
        if (FB.IsLoggedIn)
        {
            // AccessToken class will have session details
            var aToken = Facebook.Unity.AccessToken.CurrentAccessToken;
            acessToken = aToken;

            SetPlayerOnFacebook();
            // Print current access token's User ID
            Debug.Log(aToken.UserId);
            // Print current access token's granted permissions
            foreach (string perm in aToken.Permissions)
            {
                Debug.Log(perm);
            }
            FB.API("/me?fields=id,first_name", HttpMethod.GET, DealWithUserName);
        }
        else
        {
            Debug.Log("User cancelled login");
            if (OnConnectionFailed != null) OnConnectionFailed();
        }
    }

    private void DealWithUserName(IGraphResult result)
    {
        if (result.Error != null)
        {
            Debug.Log("problem with getting profile picture");

            FB.API("/me?fields=id,first_name", HttpMethod.GET, DealWithUserName);
            return;
        }

        try
        {
            userName = result.ResultDictionary["first_name"].ToString();
            userid = result.ResultDictionary["id"].ToString();
            userImage = null;
            PlayerPrefs.SetString("username", userName);
            PlayerPrefs.SetString("userid", userid);
        }
        catch (Exception e)
        {
            Debug.Log(e);
        }
    }

    IEnumerator GetDownloadURL(string id, float pixelsPerUnit = 100.0f)
    {
        string url = "https://graph.facebook.com/" + id + "/picture?type=large";
        url = url + "&redirect=false";
        WWW www = new WWW(url);
        yield return www;
        Dictionary<string, object> dict = Json.Deserialize(www.text) as Dictionary<string, object>;
        Dictionary<string, object> dataDict = dict["data"] as Dictionary<string, object>;
        StartCoroutine(DownloadImage(dataDict["url"].ToString()));
    }

    IEnumerator DownloadImage(string url)
    {
        WWW www = new WWW(url);
        yield return www;
        if (www.error == null)
        {
            Texture2D texture = www.texture;
            userImage = Sprite.Create(texture, new Rect(0, 0, 128, 128), new Vector2(.5f, .5f));
        }
        ///...
    }


    public void StartToGetImages(Dictionary<string,TopScoreUser> dict)
    {
        StartCoroutine(GetProfilepicutre(dict));
    }


    public IEnumerator GetProfilepicutre(Dictionary<string,TopScoreUser> dictonary)
    {
        string url = "https://graph.facebook.com/";
        string append = "/picture ? type = large & width = 128 & height = 128 & redirect = false";
        List<string> ids = new List<string>(dictonary.Keys);
        for(int i=0;i<ids.Count;i++)
        {
            if(dictonary[ids[i]].img == null)
            {
                WWW www = new WWW(url+ids+append);
                yield return www;
                Dictionary<string, object> dict = Json.Deserialize(www.text) as Dictionary<string, object>;
                Dictionary<string, object> dataDict = dict["data"] as Dictionary<string, object>;

                WWW wwwImage = new WWW(dataDict["url"].ToString());
                yield return wwwImage;
                if (www.error == null)
                {
                    Texture2D texture = www.texture;
                    dictonary[ids[i]].img = Sprite.Create(texture, new Rect(0, 0, 128, 128), new Vector2(.5f, .5f));
                    if(ids[i]== userid)
                    {
                        SaveMyUserImage(texture);
                    }
                }
            }
        }
    }

    

    public bool IsLogIn()
    {
        return FB.IsLoggedIn;
    }
}


