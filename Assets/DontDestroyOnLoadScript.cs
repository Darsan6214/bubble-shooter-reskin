﻿using UnityEngine;
using System.Collections;

public class DontDestroyOnLoadScript : MonoBehaviour {
    public static DontDestroyOnLoadScript instance;
    public GameScriptable gameScriptable;
    void Awake()
    {
        if(instance == null)
        {
            instance = this;
            if(gameScriptable.hasOffer)
            {
                PrefManager.SetOffer(gameScriptable.delay.days, gameScriptable.delay.hours, gameScriptable.delay.minutes);
                gameScriptable.hasOffer = false;
            }
            if (PrefManager.Rate.GetRateMeCountDown() != gameScriptable.count)
            PrefManager.Rate.SetRateMeCountDown(gameScriptable.count);
        }
        else if(instance != this)
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);
    }
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
